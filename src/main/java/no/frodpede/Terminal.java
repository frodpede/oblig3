package no.frodpede;

public class Terminal {
    HandOfCards hand = new HandOfCards();
    DeckOfCards deck = new DeckOfCards();



    /**
     * Trekker 5 kort for spiller
     */
    public void play() {
        hand.addCards(deck.dealHand(5));
    }

        /**
         * Finner kort på gitt index
         * @param index
         * @return et kort
         */
        public PlayingCard playerHandAt ( int index){
            String kort = " ";
            return hand.getCardsInHand().get(index);
        }
}

