package no.frodpede;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.Collection;

public class HandOfCards {
    private ArrayList<PlayingCard> cardsInHand; //collection of cards in the hand.

    /**
     * Constructor that initializes the cardsInHand-list and creates a HandOfCards-object.
     */
    public HandOfCards() {
        cardsInHand = new ArrayList<>(5);
    }

    /**
     * Checks if the hand is a flush. More specifically: checks if there is exactly one unique suit in the hand.
     * Ideally, this method would require exactly five of one suit, but this works for the purposes of this project.
     *
     * @return true if flush, false if not.
     */
    public boolean isFlush() {
        return cardsInHand.stream().map(PlayingCard::getSuit).distinct().count() == 1;
    }

    /**
     * Adds a collection of cards to the hand. Fails if the total amount of cards exceeds 5 cards.
     *
     * @param cards list of cards to add.
     * @throws IllegalArgumentException if adding too many cards.
     */
    public void addCards(Collection<PlayingCard> cards) throws IllegalArgumentException {
        if (cardsInHand.size() + cards.size() > 5) {
            throw new IllegalArgumentException("Hand can only contain 5 cards");
        }
        cardsInHand.addAll(cards);
    }

    /**
     * Gets the heart in the hand as a string.
     *
     * @return getHearts as string.
     */
    public String getHeartsAsString() {
        return getHearts().stream().map(PlayingCard::getAsString).collect(Collectors.joining(" "));
    }

    public int getSize() {
        return cardsInHand.size();
    }

    /**
     * Clears the hand / removes all cards.
     */
    private void clear() {
        cardsInHand = new ArrayList<>(5);
    }

    /**
     * Checks if the hand contains the queen of spades.
     *
     * @return true if it contains queen of spades, false if not.
     */
    public boolean isQueenOfSpades() {
        return cardsInHand.stream().anyMatch(c -> c.getAsString().equals("S12"));
    }

    /**
     * Sums up all the faces in the hand and returns it.
     *
     * @return the sum of faces in hand.
     */
    public int getSumFaces() {
        return cardsInHand.stream().map(PlayingCard::getFace).reduce(0, Integer::sum);
    }

    /**
     * Filters out all cards of hearts in the hand and returns them.
     *
     * @return cards of heart.
     */
    public ArrayList<PlayingCard> getHearts() {
        return (ArrayList<PlayingCard>) cardsInHand.stream().filter(c -> c.getSuit() == 'H').collect(Collectors.toList());
    }

    /**
     * Returns the cards in the hand and clears the hand.
     *
     * @return the cards in the hand.
     */
    public ArrayList<PlayingCard> returnCards() {
        ArrayList<PlayingCard> cards = cardsInHand;
        clear();
        return cards;
    }

    public ArrayList<PlayingCard> getCardsInHand() {
        return cardsInHand;
    }

    /**
     * Returns the cards in the hand as a string.
     *
     * @return hand as a string.
     */
    public String getAsString() {
        return cardsInHand.stream().map(PlayingCard::getAsString).collect(Collectors.joining(" "));
    }
}
