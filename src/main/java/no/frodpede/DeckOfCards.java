package no.frodpede;

import java.util.ArrayList;
import java.util.Random;

/**
 * Class that represent a complete deck of card(52 cards),
 * and including functions.
 *
 * @author Frode
 */

public class DeckOfCards {
    private final static char[] suit = {'S', 'H', 'D', 'C'};
    private final ArrayList<PlayingCard> cards;
    private final Random random = new Random();

    public DeckOfCards() {
        cards = new ArrayList<>(52);
        for (int i = 1; i<13; i++) {
            for (int j = 0; j<4; j++) {
                cards.add(new PlayingCard(suit[j],i));
            }
        }
    }



    public ArrayList<PlayingCard> dealHand(int n) throws IllegalArgumentException {
        if (n > 52 || n < 1) {
            throw new IllegalArgumentException("Choose a number from 1 to 52 "); }

        ArrayList<PlayingCard> dealtCards = new ArrayList<>();
        while (n>0) {
            int index = random.nextInt(cards.size());
            dealtCards.add(cards.get(index));
            cards.remove(index);
            n--;
        }
        return dealtCards;
    }

    public void reset() {
        cards.clear();
    }

    public static char[] getSuit() {
        return suit;
    }
    public ArrayList<PlayingCard> getCards() {
        return cards;
    }
    public int getDeckSize() {
        return cards.size();
    }
}